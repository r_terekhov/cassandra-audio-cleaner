package tech.ivoice.platform.audiocleaner.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Set;

@Data
@Component
@ConfigurationProperties(prefix = "props")
public class Properties {
    /*host:port*/
    private Set<String> ctsAddresses;
    private Boolean removeEnable;
    private Boolean cleanupEnable;
    private Boolean migrationEnable;
}
