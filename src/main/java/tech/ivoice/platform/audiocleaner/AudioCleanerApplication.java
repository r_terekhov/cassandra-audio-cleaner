package tech.ivoice.platform.audiocleaner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AudioCleanerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AudioCleanerApplication.class, args);
    }

}
