package tech.ivoice.platform.audiocleaner.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AudiosChunkResponse {
    private List<Audio> audiosChunk;
    private Boolean hasNextChunk;

    @Override
    public String toString() {
        return "AudiosChunkResponse{" +
                "audiosChunk=" + audiosChunk +
                ", hasNextChunk=" + hasNextChunk +
                '}';
    }
}
