package tech.ivoice.platform.audiocleaner.model;

import com.datastax.driver.core.utils.UUIDs;
import lombok.Data;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Objects;

@Table("audios")
@Data
public class Audio {

    @Id
    @PrimaryKey
    @Getter
    String id;
    String client;
    String scenario;
    /**
     * audio name inside scenario; not unique (unique is client+scenario+name)
     */
    String name;
    /**
     * if no other info provided, then audio = 8-khz, 16-bit (wav Microsoft 16-bit PCM)
     */
    ByteBuffer audio;
    Map<String, String> meta;

    @PersistenceConstructor
    private Audio() {
    }

    public Audio(String client, String scenario, String name, byte[] audio,
                 Map<String, String> meta) {
        this.id = UUIDs.timeBased().toString();
        this.client = client;
        this.scenario = scenario;
        this.name = name;
        this.audio = ByteBuffer.wrap(audio);
        this.meta = meta;
    }

    public Audio(String id, String client, String scenario, String name, byte[] audio, Map<String, String> meta) {
        this.id = id;
        this.client = client;
        this.scenario = scenario;
        this.name = name;
        this.audio = ByteBuffer.wrap(audio);
        this.meta = meta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Audio)) return false;
        Audio audio = (Audio) o;
        return getId().equals(audio.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Audio{" +
                "id='" + id + '\'' +
                '}';
    }
}