package tech.ivoice.platform.audiocleaner.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.cassandra.core.query.CassandraPageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import tech.ivoice.platform.audiocleaner.model.Audio;
import tech.ivoice.platform.audiocleaner.model.AudiosChunkResponse;
import tech.ivoice.platform.audiocleaner.properties.Properties;
import tech.ivoice.platform.audiocleaner.repository.AudioRepository;
import tech.ivoice.platform.audiocleaner.service.MigrationService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MigrationServiceImpl implements MigrationService {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationService.class);
    private final Properties properties;
    private final AudioRepository audioRepository;
    private final UtilsService utilsService;
    private final List<Audio> dataForMigration;
    private int currentIndex = 0;

    private static final int PAGE_SIZE = 10;

    public MigrationServiceImpl(Properties properties, AudioRepository audioRepository, UtilsService utilsService) {
        this.properties = properties;
        this.audioRepository = audioRepository;
        this.utilsService = utilsService;
        this.dataForMigration = new ArrayList<>();
    }


    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        prepareDataForMigration();
    }

    /**
     * node1 run this
     */
    @Override
    public void prepareDataForMigration() {
        if (!properties.getMigrationEnable()) {
            return;
        }

        long willMigratedAudiosCounter = 0;
        long totalAudiosCounter = 0;
        Set<String> scenarios = loadScenarios();
        Slice<Audio> audioSlice = audioRepository.findAll(CassandraPageRequest.of(0, PAGE_SIZE));

        do {
            for (Audio audio : audioSlice.getContent()) {
                totalAudiosCounter++;
                if (scenarios.contains(audio.getScenario())) {
                    dataForMigration.add(audio);
                    willMigratedAudiosCounter++;
                }
                LOG.info("audios will be migrated {}", willMigratedAudiosCounter);
                LOG.info("total audios {}", totalAudiosCounter);

            }

            if (audioSlice.hasNext()) {
                audioSlice = audioRepository.findAll(audioSlice.nextPageable());
            } else {
                break;
            }
        } while (!audioSlice.getContent().isEmpty());

        LOG.info("FINAL audios will be migrated {}", willMigratedAudiosCounter);
        LOG.info("FINAL total audios {}", totalAudiosCounter);
    }

    @Override
    public AudiosChunkResponse getAudiosChunk(int index, int size) {
        boolean hasNext = (index + size) < dataForMigration.size();
        List<Audio> subList = dataForMigration.subList(index, Math.min(index + size, dataForMigration.size()));
        return new AudiosChunkResponse(subList, hasNext);
    }

    /**
     * node2 run this
     */
    @Override
    public void consumeDataFromAnotherNode() {
        int migratedCounter = 0;
        int size = 10;
        AudiosChunkResponse audiosChunkResponse;
        do {
            audiosChunkResponse = utilsService.loadAudiosChunk(currentIndex, size);
            LOG.info("currentIndex is {}, loaded chunk is {}", currentIndex, audiosChunkResponse);
            for (Audio audio : audiosChunkResponse.getAudiosChunk()) {
                audioRepository.save(audio);
                LOG.info("audio with id {} saved in repo, migrated counter is {}", audio.getId(), migratedCounter);
                migratedCounter++;
            }
            currentIndex += size;
        } while (audiosChunkResponse.getHasNextChunk());
    }

    private Set<String> loadScenarios() {
        Set<String> scenarioIds = new HashSet<>();
        properties.getCtsAddresses().forEach(ctsHostPort -> scenarioIds.addAll(utilsService.loadScenariosFromCts(ctsHostPort)));
        LOG.info("loaded scenarios: {}", String.join(",", scenarioIds));
        return scenarioIds;
    }
}
