package tech.ivoice.platform.audiocleaner.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.cassandra.core.query.CassandraPageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import tech.ivoice.platform.audiocleaner.model.Audio;
import tech.ivoice.platform.audiocleaner.properties.Properties;
import tech.ivoice.platform.audiocleaner.repository.AudioRepository;
import tech.ivoice.platform.audiocleaner.service.CleanUpService;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Service
public class CleanUpServiceImpl implements CleanUpService {

    private static final Logger LOG = LoggerFactory.getLogger(CleanUpService.class);
    private final UtilsService utilsService;
    private final Properties properties;
    private final AudioRepository audioRepository;
    private static final int PAGE_SIZE = 100;

    public CleanUpServiceImpl(UtilsService utilsService, Properties properties, AudioRepository audioRepository) {
        this.utilsService = utilsService;
        this.properties = properties;
        this.audioRepository = audioRepository;
    }

    @Scheduled(cron = "0 0 1 * * FRI")
    private void scheduledClean() {
        LOG.info("started scheduled clean");
        cleanup();
    }

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        cleanup();
    }

    private Set<String> loadScenarios() {
        Set<String> scenarioIds = new HashSet<>();
        properties.getCtsAddresses().forEach(ctsHostPort -> scenarioIds.addAll(utilsService.loadScenariosFromCts(ctsHostPort)));
        LOG.info("loaded scenarios: {}", String.join(",", scenarioIds));
        return scenarioIds;
    }

    @Override
    public void cleanup() {
        if (!properties.getCleanupEnable()) {
            return;
        }
        LOG.info("start cleanup in {}", Objects.equals(properties.getRemoveEnable(), true) ? "production mode with deletion enable" : "testing mode with deletion disabled");
        long deletedAudiosCounter = 0;
        long totalAudiosCounter = 0;
        Set<String> scenarios = loadScenarios();
        Slice<Audio> audioSlice = audioRepository.findAll(CassandraPageRequest.of(0, PAGE_SIZE));

        do {
            for (Audio audio : audioSlice.getContent()) {
                totalAudiosCounter++;
                if (!scenarios.contains(audio.getScenario())) {
                    if (properties.getRemoveEnable()) {
                        audioRepository.deleteAudioById(audio.getId());
                        LOG.info("deleted audio with id {} for scenario {}", audio.getId(), audio.getScenario());
                    } else {
                        LOG.info("audio with id {} for scenario {} can be deleted", audio.getId(), audio.getScenario());
                    }
                    deletedAudiosCounter++;
                }
                LOG.info("total audios deleted {}", deletedAudiosCounter);
                LOG.info("total audios {}", totalAudiosCounter);

            }

            if (audioSlice.hasNext()) {
                audioSlice = audioRepository.findAll(audioSlice.nextPageable());
            } else {
                break;
            }
        } while (!audioSlice.getContent().isEmpty());

        LOG.info("FINAL total audios deleted {}", deletedAudiosCounter);
        LOG.info("FINAL total audios {}", totalAudiosCounter);
    }
}
