package tech.ivoice.platform.audiocleaner.service;

import tech.ivoice.platform.audiocleaner.model.AudiosChunkResponse;

public interface MigrationService {
    /*README:
     *
     * 1 node prepareDataForMigration from 1 cassandra instance
     * 2 node consume prepared data from 1 node via rest and save data to 2 cassandra instance
     *
     * */

    void prepareDataForMigration();

    AudiosChunkResponse getAudiosChunk(int index, int size);

    void consumeDataFromAnotherNode();
}
