package tech.ivoice.platform.audiocleaner.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tech.ivoice.platform.audiocleaner.model.AudiosChunkResponse;
import tech.ivoice.platform.audiocleaner.properties.Properties;

import java.util.HashSet;
import java.util.Set;

import static java.lang.String.format;

@Service
public class UtilsService {

    private static final Logger LOG = LoggerFactory.getLogger(UtilsService.class);
    private final RestTemplate restTemplate;
    private final Properties properties;


    public UtilsService(Properties properties) {
        this.properties = properties;
        this.restTemplate = new RestTemplate();
        LOG.info("loaded cts list: {}", String.join(",", properties.getCtsAddresses()));
    }

    public Set<String> loadScenariosFromCts(String ctsHostPort) {
        String url = String.format("http://%s/scenario/getList", ctsHostPort);
        try {
            ResponseEntity<Set<String>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Set<String>>() {
            });
            HttpStatus statusCode = responseEntity.getStatusCode();

            if (statusCode.is2xxSuccessful()) {
                Set<String> response = responseEntity.getBody();
                if (response == null) {
                    LOG.error("error on getting scenarios, cts {}, reason: response is null", ctsHostPort);
                    throw new RuntimeException();
                }
                return new HashSet<>(response);

            } else {
                LOG.error("error on getting scenarios, cts {}, reason:  status code is {}", ctsHostPort, statusCode.value());
                throw new RuntimeException();
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            throw new RuntimeException(format("error on getting scenarios %s", ex.getMessage()));
        }

    }

    public AudiosChunkResponse loadAudiosChunk(int index, int size) {
        //from first instance
        String url = String.format("http://192.168.0.77:8418/getAudiosChunk?index=%s&size=%s", index, size);
        try {
            ResponseEntity<AudiosChunkResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<AudiosChunkResponse>() {
            });
            HttpStatus statusCode = responseEntity.getStatusCode();

            if (statusCode.is2xxSuccessful()) {
                AudiosChunkResponse response = responseEntity.getBody();
                if (response == null) {
                    LOG.error("error on getting audios, reason: response is null");
                    throw new RuntimeException();
                }
                return response;

            } else {
                LOG.error("error on getting audios, reason:  status code is {}", statusCode.value());
                throw new RuntimeException();
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            throw new RuntimeException(format("error on getting audios chunk %s", ex.getMessage()));
        }

    }


}
