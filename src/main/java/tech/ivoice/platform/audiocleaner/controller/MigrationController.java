package tech.ivoice.platform.audiocleaner.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.ivoice.platform.audiocleaner.model.AudiosChunkResponse;
import tech.ivoice.platform.audiocleaner.service.MigrationService;

@RestController
public class MigrationController {

    private final MigrationService migrationService;

    public MigrationController(MigrationService migrationService) {
        this.migrationService = migrationService;
    }

    @GetMapping("getAudiosChunk")
    public ResponseEntity<AudiosChunkResponse> getAudiosChunk(@RequestParam int index, int size) {
        AudiosChunkResponse audiosChunkResponse = migrationService.getAudiosChunk(index, size);

        return new ResponseEntity<>(audiosChunkResponse, HttpStatus.OK);
    }

    @Async
    @GetMapping("startConsumeDataFromFirstNode")
    public ResponseEntity<Void> startConsumeDataFromFirstNode() {
        migrationService.consumeDataFromAnotherNode();
        return ResponseEntity.ok(null);
    }
}
