/*
 * Copyright 2013-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tech.ivoice.platform.audiocleaner.config;

import com.datastax.oss.driver.api.core.CqlSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import tech.ivoice.platform.audiocleaner.model.Audio;

@Configuration
@EnableAutoConfiguration
class PersistenceConfig {

	@Configuration
	@EnableCassandraRepositories
	static class CassandraConfig extends AbstractCassandraConfiguration {

		@Value("${spring.data.cassandra.contact-points}")
		private String casAddress;

		@Value("${spring.data.cassandra.keyspace-name}")
		private String casKeyspace;

		@Value("${spring.data.cassandra.port}")
		private int casPort;

		@Override
		public String getKeyspaceName() {
			return casKeyspace;
		}

		@Override
		public String getContactPoints() {
			return casAddress;
		}


		@Override
		protected int getPort() {
			return casPort;
		}

		@Bean
		public CassandraTemplate cassandraTemplate(CqlSession session) {
			return new CassandraTemplate(session);
		}

		@Override
		public String[] getEntityBasePackages() {
			return new String[]{Audio.class.getPackage().getName()};
		}

		@Override
		public SchemaAction getSchemaAction() {
			return SchemaAction.CREATE_IF_NOT_EXISTS;
		}
	}
}
