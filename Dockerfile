FROM registry.gitlab.ivoice.online/ivoice-platform/deploy-images/maven-nexus-settings:latest as builder
ADD . /app
WORKDIR /app
RUN mvn package

FROM registry.gitlab.ivoice.online/ivoice-platform/self-docker-images/alpine-java:8_jdk

COPY --from=builder /app/target/audio-cleaner.jar .
ENV LOG_LEVEL_SERVICE=INFO
ADD ./docker-entry.sh .
RUN chmod +x ./docker-entry.sh
CMD ./docker-entry.sh
EXPOSE 8625